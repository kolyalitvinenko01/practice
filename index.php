<?php 
require_once("config.php");
if (empty($_SESSION['user_id'])) {
    header("location: /login.php");
}

$stmt = $dbConn->prepare("SELECT comments.*, users.username FROM `comments` INNER JOIN `users` ON comments.user_id = users.id ORDER BY id DESC;");
$stmt->execute();
$comments = $stmt->fetchAll();
$stmt_user = $dbConn->prepare('SELECT username FROM users');
$usernames = $stmt_user->fetchAll();


?>

<!DOCTYPE html>
<html lang="en">
    <?php include "header.html" ?>
<body>
    <div class="comments-header mrleft-and-right-auto block-1">
        <h1>Posts Page</h1>
    </div>
    <div class="mrleft-and-right-auto block-1">
        <a class="btn btn-outline-primary mr-10" href="addPost.php">Add post</a>
        <a class="btn btn-outline-danger" href="logout.php">Logout</a>
    </div>
    <div class="mrleft-and-right-auto block-1">
        <h2 class="mr-20 mr-t-10">Posts:</h2>
    </div>
    <div class="comments-panel">
        <?php foreach ($comments as $comment) : ?>
        <div class="comment">
            <p <?php if ($comment['user_id'] == $_SESSION['user_id']) {
                echo 'style="font-weight: bold;"';
            }?>><span class="username"><?php echo $comment['username'];?></span>
            <span>(<?php echo $comment['created_at'];?>)</span>
            <p class="text-align-justify"><?php echo $comment['comment']?> </p>
            <?php if ($comment['user_id'] == $_SESSION['user_id']) {
                echo "<form action='delete.php' method='post'>
                <input type='hidden' name='id' value='" . $comment['id'] . "' />
                <button class='btn btn-outline-danger'>Delete</button>
                </form>
                ";   
            }?>
        </div>
        <?php endforeach; ?>
    </div>
</body>
</html>
