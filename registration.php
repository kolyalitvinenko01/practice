<?php
require_once("config.php");
if (!empty($_SESSION['user_id'])) {
    header("location: /index.php");
}

$errors = [];
if (!empty($_POST)) {
    switch (true) {
        case (empty($_POST['user_name'])):
            $errors[] = 'Please enter User Name';
            break;
        case (empty($_POST['email'])):
            $errors[] = 'Please enter email';
            break;
        case (empty($_POST['first_name'])):
            $errors[] = 'Please enter First Name';
            break;
        case (empty($_POST['last_name'])):
            $errors[] = 'Please enter Last Name';
            break;
        case (empty($_POST['password'])):
            $errors[] = 'Please enter password';
            break;
        case (empty($_POST['confirm_password'])):
            $errors[] = 'Please confirm password';
            break;
        case (strlen($_POST['user_name']) > 100):
                $errors[] = 'User Name is too long. Max length is 100 characters';
            break;
        case (strlen($_POST['first_name']) > 80):
            $errors[] = 'First Name is too long. Max length is 80 characters';
            break;
        case ((strlen($_POST['last_name']) > 150) == true):
            $errors[] = 'Last Name is too long. Max length is 150 characters';
            break;
        case (strlen($_POST['password']) < 6):
            $errors[] = 'Password should contains at least 6 characters';
            break;
        case ($_POST['password'] !== $_POST['confirm_password']):
            $errors[] = 'Your confirm password is not match password';
            break;
    }
    if (empty($errors)) {
        $stmt = $dbConn->prepare('INSERT INTO users(`username`, `email`, `password`, `first_name`, `last_name`) VALUES(:username, :email, :password, :first_name, :last_name)');
        $stmt->execute(array('username' => $_POST['user_name'], 'email' => $_POST['email'], 'password' => sha1($_POST['password'].SALT),
                            'first_name' => $_POST['first_name'], 'last_name' => $_POST['last_name']));
        header("location: /login.php?registration=1");
    }
}

?>

<!DOCTYPE html>
<html lang="en">
    <?php include "header.html" ?>
<body>
    <div class="d-flex justify-content-center center-page flex-direction-column">
        <h1>Registration Page</h1>
        <form method="POST">
            <div style="color: red;">
                <?php foreach ($errors as $error) :?>
                    <p><?php echo $error;?></p>
                <?php endforeach; ?>
            </div>
            <div>
                <label>User Name:</label><br>
                <div>
                    <input type="text" name="user_name" required="" value="<?php echo (!empty($_POST['user_name']) ? $_POST['user_name'] : "");?>">
                </div>
            </div>
            <div>
                <label>Email:</label><br>
                <div>
                    <input type="email" name="email" required="" value="<?php echo (!empty($_POST['email']) ? $_POST['email'] : "");?>">
                </div>    
            </div>
            <div>
                <label>First Name:</label><br>
                <div>
                    <input type="text" name="first_name" required="" value="<?php echo (!empty($_POST['first_name']) ? $_POST['first_name'] : "");?>">
                </div>
            </div>
            <div>
                <label>Last Name:</label><br>
                <div>
                    <input type="text" name="last_name" required="" value="<?php echo (!empty($_POST['last_name']) ? $_POST['last_name'] : "");?>">
                </div>
            </div>
            <div>
                <label>Password:</label><br>
                <div>
                    <input type="password" name="password" required="" value=""/>
                </div>
            </div>
            <div>
                <label>Confirm password:</label><br>
                <div>
                    <input type="password" name="confirm_password" required="" value=""/>
                </div>
            </div>
            <div class="mr-t-20">
                <input type="submit" name="submit" class="btn btn-outline-primary" value="Register">
            </div>
        </form>
    </div>
</body>
</html>