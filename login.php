<?php 
require_once("config.php");
if (!empty($_SESSION['user_id'])) {
    header("location: /index.php");
}

$errors = [];
$isRegistered = 0;
$isAuthorized = 0;
if (!empty($_GET['registration'])) {
    $isRegistered = 1;
}
if (!empty($_POST)) {
    if (empty($_POST['user_name'])) {
        $errors[] = 'Please enter Email';
    } elseif (empty($_POST['password'])) {
        $errors[] = 'Please enter password';
    } else {
        $stmt = $dbConn->prepare('SELECT id FROM users WHERE (email = :username) and password = :password');
        $stmt->execute(array('username' => $_POST['user_name'], 'password' => sha1($_POST['password'].SALT)));
        $id = $stmt->fetchColumn();
        if (!empty($id)) {
            $_SESSION['user_id'] = $id;
            //die("Вы успешно авторизинованны");
            header("location: /index.php");
        } else {
            $errors[] = 'Please enter valid credentials';
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
    <?php include "header.html" ?>
<body>
    <div class="d-flex justify-content-center center-page flex-direction-column">
        <h1>Log In Page</h1>
        <form method="POST">
            <div style="color: red;">
                <?php foreach ($errors as $error) :?>
                    <p><?php echo $error;?></p>
                <?php endforeach; ?>
                <?php if(!empty($isRegistered)) :?>
                    <p>Вы успешно зарегестрировались! Используйте свои данные для входа на сайт</p>
                <?php endif; ?>
            </div>
            <div>
                <label>Email:</label>
                <div>
                    <input type="text" name="user_name" required="" value="<?php echo (!empty($_POST['user_name']) ? $_POST['user_name'] : '');?>"/>
                </div>
            </div>
            <div>
                <label>Password:</label>
                <div>
                    <input type="password" name="password" required="" value=""/>
                </div>
            </div>
            <div class="block-2">
                <br/>
                <input type="submit" name="submit" class="btn btn-outline-primary" value="Log In">
            </div>      
        </form>
    </div>
</body>
</html>