-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 03 2022 г., 22:32
-- Версия сервера: 5.5.62
-- Версия PHP: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `guestbook`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `comment`, `created_at`) VALUES
(19, 12, 'Ще не вмерла Україна, і слава, і воля,\r\nЩе нам, браття молодії, усміхнеться доля.\r\nЗгинуть наші вороженьки, як роса на сонці,\r\nЗапануєм і ми, браття, у своїй сторонці.', '2022-06-03 16:46:06'),
(20, 13, 'Станем, браття, в бій кровавий від Сяну до Дону\r\nВ ріднім краю панувати не дамо нікому;\r\nЧорне море ще всміхнеться, дід Дніпро зрадіє,\r\nЩе у нашій Україні доленька наспіє.', '2022-06-03 17:08:57'),
(21, 14, 'А завзяття, праця щира свого ще докаже,\r\nЩе ся волі в Україні піснь гучна розляже,\r\nЗа Карпати відоб\'ється, згомонить степами,\r\nУкраїни слава стане поміж народами.', '2022-06-03 17:09:48'),
(22, 15, '\"Ще не вмерла України** і слава, і воля,\r\nЩе нам, браття молодії, усміхнеться доля.\r\nЗгинуть наші воріженьки, як роса на сонці.\r\nЗапануєм і ми, браття, у своїй сторонці.', '2022-06-03 17:10:15'),
(23, 16, 'Душу, тіло ми положим за нашу свободу.\r\nІ покажем, що ми, браття, козацького роду.', '2022-06-03 17:12:58'),
(24, 17, 'Станем, браття, в бій кровавий від Сяну до Дону\r\nВ ріднім краю панувати не дамо нікому;\r\nЧорне море ще всміхнеться, дід Дніпро зрадіє,\r\nЩе у нашій Україні доленька наспіє.', '2022-06-03 17:13:35');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(100) NOT NULL,
  `first_name` varchar(80) NOT NULL,
  `last_name` varchar(80) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `first_name`, `last_name`, `created_at`) VALUES
(12, 'Nikolay', 'test1@gmail.com', 'b13232e23a545f592eb9d4c0d4227bc611902671', 'Nikolay', 'Litvinenko', '2022-06-03 16:42:19'),
(13, 'Chel', 'test1@gmail.com', '19d537a5ef3079c3b20ddbd01e457d4c50030c7e', 'Chel', 'You', '2022-06-03 16:53:07'),
(14, 'Bot1', 'test2@gmail.com', '6bfb7df69ede9396250d26c3dc8c095f162c3be4', 'Bot', 'he', '2022-06-03 16:54:06'),
(15, 'Bot2', 'test3@gmail.com', 'c4e613035951cc238251b624ba5a091774682398', 'Bot2', 'she', '2022-06-03 16:57:25'),
(16, 'Bot3', 'test4@gmail.com', 'dc4c2d718166a9653b087fbe8537b82ca113af4b', 'Bot3', 'it', '2022-06-03 16:58:53'),
(17, 'Bot4', 'test6@gmail.com', 'f786a57ae7cb8774d614939fcb18e9760bf2293a', 'Bot4', 'they', '2022-06-03 17:02:23'),
(18, 'Alex', 'alex@gmail.loc', 'b13232e23a545f592eb9d4c0d4227bc611902671', 'Alex', 'Tyschenko', '2022-06-03 19:09:14');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
