<?php 
require_once("config.php");
if (empty($_SESSION['user_id'])) {
    header("location: /login.php");
}

if (!empty($_POST['comment'])) {
    $stmt = $dbConn->prepare('INSERT INTO comments(`user_id`, `comment`) VALUES(:user_id, :comment)');
    $stmt->execute(array('user_id' => $_SESSION['user_id'], 'comment' => $_POST['comment']));
    header("location: index.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include "header.html" ?>
</head>
<body>
    <div class="center-page w-50">
        <div class="comments-header mrleft-and-right-auto">
            <h1>Please add your post</h1>
            <a class="btn btn-outline-danger" href="logout.php">Logout</a>
        </div>
        <div class="comments-form mrleft-and-right-auto">
            <form method="POST">
                <div>
                    <label>Comment</label>
                    <div>
                        <textarea name="comment"></textarea>
                    </div>
                </div>
                <div>
                    <input type="submit" name="submit" class="btn btn-outline-primary" value="Save">
                </div>
            </form>
        </div>
    </div>
</body>
</html>